const express = require('express')
const db = require('../db')
const utils = require('../utils')

const router = express.Router()

router.get('/', (requsest, response)=>{
    const { bookTitle } = requsest.body
    const query = `
    SELECT 
        book_id, book_title, publisher_name, author_name
    FROM Book WHERE
        book_title = '${bookTitle}'
    `
    db.execute(query, (error, result)=>{
        response.send(utils.createResult(error, result))
    })
})

router.post('/', (requsest, response)=>{
    const { bookTitle, publisherName, authorName } = requsest.body
    const query = `
    INSERT INTO Book
        (book_title, publisher_name, author_name)
    VALUES
        ('${bookTitle}', '${publisherName}', '${authorName}')
    `
    db.execute(query, (error, result)=>{
        response.send(utils.createResult(error, result))
    })
})

router.put('/:id', (requsest, response)=>{
    const { id } = requsest.params
    const { bookTitle, publisherName, authorName } = requsest.body
    const query = `
    UPDATE Book
    SET
        book_title = '${bookTitle}',
        publisher_name= '${publisherName}',
        author_name= '${authorName}'
    WHERE 
        book_id= '${id}'
    `
    db.execute(query, (error, result)=>{
        response.send(utils.createResult(error, result))
    })
})

router.delete('/:id', (requsest, response)=>{
    const { id } = requsest.params
    const query = `
    DELETE FROM Book
    WHERE 
        book_id= '${id}'
    `
    db.execute(query, (error, result)=>{
        response.send(utils.createResult(error, result))
    })
})
module.exports = router
